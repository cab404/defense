package com.cab404.defense.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public abstract class SelectableField extends TextField {

    private boolean wasTouched;
    public boolean isDead = false;
    public boolean isMouseOver = false;
    // Временная фигня. Можно списывать весь класс, т.к потом фичи с удержал - навёл - отпустил - выбралось не будет.
    // В принципе, отвечает за то, как будет существовать поле:
    // true :  Поле просуществует до того момента, когда мышку/тачпад перестанут нажимать. Если нажимать перестали
    //         на кнопке (навели-отпустили), то вызывается onSelect. Идеально для мелких in-game меню, или их подуровней.
    // false : Поле действует как обычная кнопка, onSelect вызывается по щелчку.
    public boolean fast_selection = false;

    public abstract void onSelect(Vector2 mouse);

    @Override
    public boolean isDead() {
        return isDead || (fast_selection && !Gdx.input.isTouched());
    }


    @Override
    public void onMouseOver(Vector2 mouse_pos) {
        isMouseOver = true;
        bg.setColor(1, 1, 1, 0.9f);
        if (fast_selection) {
            if (!Gdx.input.isTouched()) {
                onSelect(mouse_pos);
            }
        } else if (Gdx.input.justTouched()) {
            onSelect(mouse_pos);
        }
    }


    @Override
    public String getText() {
        return "Nothing";
    }


    @Override
    public void update() {
        if (!isMouseOver) {
            bg.setColor(1, 1, 1, 0.5f);
        } else {
            isMouseOver = false;
        }
    }

}
