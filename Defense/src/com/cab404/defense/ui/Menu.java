package com.cab404.defense.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.primitives.GameObj;

import java.util.Iterator;

public class Menu extends GameObj {

    public float distance_between_fields = 1;
    public Array<Field> fields;
    private Vector2 real_size = new Vector2();


    public Menu() {
        super();
        var.is_ui = true;
        fields = new Array<>();
    }


    @Override
    public void draw(SpriteBatch batch) {
        Iterator<Field> iter = fields.iterator();

        Vector2 drawingPoint = var.pos.cpy();
        Vector2 menu_size = new Vector2();

        while (iter.hasNext()) {
            Field proc = iter.next();
            Vector2 size = proc.getSize();
            proc.draw(batch, drawingPoint);

            drawingPoint.y += size.y + distance_between_fields;
            menu_size.y += size.y + distance_between_fields;

            menu_size.x = Math.max(size.x, menu_size.x);
        }
        real_size.set(menu_size);

    }


    @Override
    public void onMouseOver(Vector2 mouse) {
        Iterator<Field> iter = fields.iterator();
        Vector2 drawingPoint = var.pos.cpy();

        while (iter.hasNext()) {
            Field tmp = iter.next();
            Vector2 size = tmp.getSize();

            Rectangle rect = new Rectangle();
            rect.set(drawingPoint.x, drawingPoint.y, size.x, size.y);

            if (rect.contains(mouse.x, mouse.y)) {
                tmp.onMouseOver(mouse);
                break;
            }

            drawingPoint.y += size.y + distance_between_fields;
        }
    }


    /**
     * Запускает update во всех полях
     */
    public void updateFields() {
        Iterator<Field> iter = fields.iterator();
        while (iter.hasNext()) {
            Field tmp = iter.next();
            if (tmp.isDead()) {
                iter.remove();
                if (fields.size == 0) {
                    var.hp = 0;
                }
            } else {
                tmp.update();
            }
        }
    }


    @Override
    public void update() {
        // Зачем я вынес updateFields в отдельный метод? Без понятия.
        updateFields();

        var.size.set(real_size.x, real_size.y);
        var.pos.set(var.pos.x, var.pos.y);

    }

}
