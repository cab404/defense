package com.cab404.defense.storage;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.util.Def;
import com.cab404.defense.util.Graphics;
import javolution.util.FastMap;

import java.util.Iterator;
import java.util.Map;

/**
 * Хранилище для объектов.
 *
 * @author cab404
 */
public class ObjectStorage implements Iterable {

    /**
     * А еще у нас будет магическая команда цвета null, на которую ничто не будет агрится.
     */
    private FastMap<Color, Array<GameObj>> commands;
    public Graphics gfx;

    public ObjectStorage() {
        commands = new FastMap<>();
        gfx = new Graphics();
    }

    /**
     * Добавляет объект в хранилище по флагу команды team, выставляя флаг в stats
     */
    public void addObjectToCommand(Color command, GameObj object) {
        object.stats.team = command;
        object.var.parent = this;
        if (!commands.containsKey(command))
            commands.put(command, new Array<GameObj>());
        commands.get(command).add(object);

    }

    /**
     * Добавляет объект в хранилище по выставленному флагу
     */
    public void add(GameObj object) {
        object.var.parent = this;
        if (!commands.containsKey(object.stats.team))
            commands.put(object.stats.team, new Array<GameObj>());
        commands.get(object.stats.team).add(object);

    }

    public void remove(GameObj obj) {
        if (commands.containsKey(obj.stats.team)) {
            commands.get(obj.stats.team).removeValue(obj, true);
        }
    }

    public Array<GameObj> getAllTeamsExcept(Color exception) {
        Array<GameObj> all_objects = new Array<>();
        Iterator<Color> keys = commands.keySet().iterator();

        for (Map.Entry<Color, Array<GameObj>> entry : commands.entrySet()) {
            if (!entry.getKey().equals(Color.WHITE) && !entry.getKey().equals(exception))
                all_objects.addAll(entry.getValue());
        }

        return all_objects;

    }

    public Array<GameObj> getAll() {
        Array<GameObj> all_objects = new Array<>();
        for (Array<GameObj> gameObjects : commands.values()) {
            all_objects.addAll(gameObjects);
        }

        return all_objects;

    }


    public void update(int iterations, float time) {
        for (int i = iterations; i != 0; i--) {
            update(time / iterations);
        }
    }


    private void update(float time) {
        gfx.update();
        GameObj.time = time;
        Array<GameObj> toUpdate = getAll();
        Iterator<GameObj> update = toUpdate.iterator();

        Vector2 mouse = gfx.convertToGFX(Def.mouse(0));

        while (update.hasNext()) {
            GameObj tmp = update.next();

            if (tmp.var.hp > 0) {
                tmp.internalUpdate();

                if (tmp.var.getBounds().contains(mouse.x, mouse.y))
                    tmp.onMouseOver(mouse);

                tmp.update();
            } else {

                remove(tmp);
                tmp.onDeath();
                tmp.var.isDead = true;
                update.remove();

            }
        }


    }


    public void render() {
        gfx.batch.begin();
        for (GameObj toRender : getAll()) {
            toRender.draw(gfx.batch);
        }
        gfx.batch.end();
    }

    @Override
    public Iterator iterator() {
        return commands.values().iterator();
    }
}
