package com.cab404.defense.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.net.ServerConnection;
import com.cab404.defense.storage.ObjectStorage;
import com.cab404.defense.util.Def;

/**
 * Сцена с возможностью прорисовки двух слоёв - игры и UI.
 *
 * @author cab404
 */
public class GameScene extends Scene {

    public ObjectStorage game;
    public float time_zoom = 1;
    ServerConnection conn;

    @Override
    public void create() {
        super.create();
        conn = new ServerConnection("127.0.0.1", 10100);
        game = new ObjectStorage();
    }

    @Override
    public void dispose() {
        super.dispose();
        conn.dispose();
    }

    @Override
    public void resize(Vector2 size) {
        super.resize(size);
    }

    @Override
    public void render() {
        super.render();
        game.render();
    }

    @Override
    public void update() {
        game.update(1, Gdx.graphics.getDeltaTime() * time_zoom);
        super.update();
    }

    public class GameSceneInputProcessor implements InputProcessor {
        @Override
        public boolean keyDown(int keycode) {
            return false;
        }


        @Override
        public boolean keyUp(int keycode) {
            return false;
        }


        @Override
        public boolean keyTyped(char character) {
            return false;
        }


        @Override
        public boolean touchDown(int screenX, int screenY, int pointer, int button) {
            return false;
        }


        @Override
        public boolean touchUp(int screenX, int screenY, int pointer, int button) {
            return false;
        }


        private float last_dst = 0;
        private Vector2 last_center;

        @Override
        public boolean touchDragged(int screenX, int screenY, int pointer) {
            if (Gdx.input.isTouched(0) && Gdx.input.isTouched(1)) {

                Vector2 p1 = Def.mouse(0);
                Vector2 p2 = Def.mouse(1);
                Vector2 center = p1.cpy().add(p2.cpy().sub(p1).div(2));

                float dst = p1.dst(p2);
                if (last_dst == 0) {
                    last_center = center;
                    last_dst = dst;
                }

                float zoom_mn = (last_dst / dst - 1);// / cam.zoom;

                Vector2 pos_1 = game.gfx.convertToGFX(center);
                OrthographicCamera cam = game.gfx.cam;

                cam.zoom /= 1 + zoom_mn;
                cam.zoom = cam.zoom > 0.1f ? cam.zoom : 0.1f;
                cam.zoom = cam.zoom < 10f ? cam.zoom : 10f;

                Vector2 pos_2 = game.gfx.convertToGFX(center);

                cam.translate(pos_1.sub(pos_2));
                cam.translate(game.gfx.convertToGFX(last_center).sub(game.gfx.convertToGFX(center)));

                last_dst = dst;
                last_center = center.cpy();
            } else {

                last_dst = 0;

            }
            return false;
        }


        @Override
        public boolean mouseMoved(int screenX, int screenY) {
            return false;
        }


        @Override
        public boolean scrolled(int amount) {
            // Довольно просто - сохраняем положение
            // курсора до зума и после, разницу добавляем к положению камеры, и
            // вуаля - курсор при зуме всегда на одной точке.

            Vector2 mouse_pos_1 = game.gfx.convertToGFX(Def.mouse(0));
            game.gfx.cam.zoom += amount * 0.1f;
            game.gfx.cam.zoom = game.gfx.cam.zoom > 0.1f ? game.gfx.cam.zoom : 0.1f;
            Vector2 mouse_pos_2 = game.gfx.convertToGFX(Def.mouse(0));

            game.gfx.cam.translate(mouse_pos_1.sub(mouse_pos_2));

            return true;
        }

    }

}
