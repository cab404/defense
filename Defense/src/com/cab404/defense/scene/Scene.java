package com.cab404.defense.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.storage.ObjectStorage;
import com.cab404.defense.storage.SpriteStorage;

public class Scene {

    public ObjectStorage objects;
    public SpriteStorage sprites;


    public static Vector2 scr;


    public void dispose() {
        sprites.dispose();
    }


    public void create() {
        objects = new ObjectStorage();
        sprites = new SpriteStorage();
    }

    public void resize(Vector2 size) {
    }


    public void update() {
        objects.update(1, Gdx.graphics.getDeltaTime());
    }


    public void render() {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        objects.render();
    }

}
