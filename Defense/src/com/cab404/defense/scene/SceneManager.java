package com.cab404.defense.scene;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.storage.SpriteStorage;
import com.cab404.defense.util.Def;

/**
 * Класс управления сценами.
 *
 * @author cab404
 */
public class SceneManager implements ApplicationListener {
    // Кому нужны две активные сцены?
    protected Scene activeScene;
    private boolean isPaused;

    public SceneManager(Scene startScene) {
        activeScene = startScene;
    }

    public boolean isPaused() {
        return isPaused;
    }


    @Override
    public void create() {

        Scene.scr = new Vector2();
        Scene.scr.x = Gdx.graphics.getWidth();
        Scene.scr.y = Gdx.graphics.getHeight();

        // TODO: Заменить этот ужас на полноценный FontManager;
        FreeTypeFontGenerator ftf;

        ftf = new FreeTypeFontGenerator(
                Gdx.files.internal("data/ubuntu-mono.ttf"));
        SpriteStorage.font = ftf.generateFont(16, SpriteStorage.chars, false);

        activeScene.create();

        dump_to_def();
    }


    @Override
    public void resize(int w, int h) {
        Scene.scr.set(new Vector2(w, h));
        activeScene.resize(new Vector2(w, h));
    }


    @Override
    public void render() {
        if (!isPaused) {
            activeScene.update();
            activeScene.render();
        }
    }


    @Override
    public void pause() {
        isPaused = true;
    }


    @Override
    public void resume() {
        isPaused = false;
    }


    @Override
    public void dispose() {
        activeScene.dispose();
    }

    public void changeScene(Scene target) {
        dispose();

        activeScene = target;

        target.create();
        target.resize(Scene.scr);

        dump_to_def();
    }

    public SpriteStorage sprites() {
        return activeScene.sprites;
    }

    /**
     * Простая функция, скидывающая значения из сцены в глобальные статичные переменные.
     */
    private void dump_to_def() {
        Def.sprites = activeScene.sprites;
    }

    public Scene getCurrentScene() {
        return activeScene;
    }

}
