package com.cab404.defense.util;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.game.Variables;

/**
 * @author cab404
 */
public class SpriteUtils {

    public static Vector2 getSize(Sprite sprite) {
        return new Vector2(sprite.getWidth(), sprite.getHeight());
    }

    public static Vector2 getPosition(Sprite sprite) {
        return new Vector2(sprite.getX(), sprite.getY());
    }

    public static void sync(Variables var, Sprite sprite) {
        var.size = getSize(sprite);
        var.pos = getPosition(sprite);
        sprite.setRotation(var.way.angle() - 90);
    }

}
