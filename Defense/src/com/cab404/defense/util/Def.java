package com.cab404.defense.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.Scene;
import com.cab404.defense.scene.SceneManager;
import com.cab404.defense.storage.SpriteStorage;

/**
 * Различные статичные методы и переменные, по типу Gdx
 *
 * @author cab404
 */
public class Def {

    public static SceneManager scene;
    public static SpriteStorage sprites;

    public static void log(Object text) {
        try {
            Gdx.app.log("Luna Log", text.toString());
        } catch (NullPointerException e) {
            try {
                System.out.println("Luna Log : " + text.toString());
            } catch (NullPointerException ex) {
                log("null");
            }
        }
    }

    public static Vector2 mouse(int point) {
        return new Vector2(Gdx.input.getX(point), Scene.scr.y - Gdx.input.getY(point));
    }

}
