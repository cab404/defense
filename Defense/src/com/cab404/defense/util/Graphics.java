package com.cab404.defense.util;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.Scene;

/**
 * @author cab404
 */
public class Graphics {
    public SpriteBatch batch;
    public OrthographicCamera cam;

    public Graphics() {
        batch = new SpriteBatch();
        cam = new OrthographicCamera(Scene.scr.x, Scene.scr.y);
        update();
    }

    public void update() {
        cam.viewportWidth = Scene.scr.x;
        cam.viewportHeight = Scene.scr.y;
        cam.update();
        batch.setProjectionMatrix(cam.projection);
        // Я не знаю, кагого дискорда у камеры 0:0 в центре по умолчанию, но нафиг.
        batch.getProjectionMatrix().translate(-Scene.scr.x / 2 * cam.zoom, -Scene.scr.y / 2 * cam.zoom, 0);
        batch.setTransformMatrix(cam.view);
    }

    /**
     * Конвертирует координаты на экране в координаты на заданной данным классом плоскости.
     */
    public Vector2 convertToGFX(Vector2 in) {
        return in.cpy()
                .mul(cam.zoom)
                .add(cam.position.x, cam.position.y)
                ;
    }

    /**
     * Обратно функции convertToGFX
     */
    public Vector2 convertFromGFX(Vector2 in) {
        return in.cpy()
                .sub(cam.position.x, cam.position.y)
                .div(cam.zoom)
                ;
    }
}
