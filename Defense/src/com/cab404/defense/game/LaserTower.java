package com.cab404.defense.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.primitives.Particle;
import com.cab404.defense.primitives.Tower;
import com.cab404.defense.util.Def;
import com.cab404.defense.util.SpriteUtils;

public class LaserTower extends Tower {

    public static class Laser extends Particle {

        public static class TraceEmitter extends ParticleEmitter {

            public TraceEmitter(GameObj owner) {
                this.delay = 5 / owner.stats.speed;
                this.clipped_to = owner;
            }


            @Override
            public Particle getTemplate() {
                Particle prt = new Particle();
                prt.img = Def.sprites.get("main", "laser");
                prt.live = 0.5f;

                prt.var.way = clipped_to.var.way.cpy();

                prt.stats.color = Color.CYAN.cpy();
                prt.stats.speed = 0;

                return prt;
            }

        }

        public ParticleEmitter trace;


        public Laser() {
            super();
            var.way = new Vector2((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);
            SpriteUtils.sync(var, img);
        }


        @Override
        public void update() {
            move(true);
            if (var.lifetime > live)
                var.hp = 0;
        }


        @Override
        protected void move_iteration(float iter) {
            trace.step(time / iter, var.parent);
        }

    }


    public LaserTower() {
        super();

        stats.firing_speed = 0.2f;
        stats.firing_range = 1500;

        projectile_stats.rotation_speed = 0;
        projectile_stats.speed = 50000;
        projectile_stats.damage = 5;
        projectile_stats.color = Color.CYAN.cpy();
        stats.color = Color.CYAN.cpy();

        var.hp = 200;
        stats.max_hp = 200;
        var.way = new Vector2(0, 1);


    }


    @Override
    public GameObj getProjectile() {
        Laser laser = new Laser();
        centerObjectOnSelf(laser);

        laser.var.target = var.target;
        laser.var.way = var.target.var.pos.cpy().sub(laser.var.pos);

        laser.stats = projectile_stats;

        laser.trace = new Laser.TraceEmitter(laser);

        return laser;
    }


    @Override
    public void update() {
        var.target = findNearestIn(var.parent.getAllTeamsExcept(stats.team));
        sendAllRockets();
        destroyFarRockets();
    }

}
