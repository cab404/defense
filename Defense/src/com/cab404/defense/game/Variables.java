package com.cab404.defense.game;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.storage.ObjectStorage;

public class Variables {

    // Местоположение объекта.
    public Vector2 pos;

    // Направление объекта. Не влияет на скорость, см stats.speed.
    public Vector2 way;

    // Размер объекта
    public Vector2 size;

    // Цель, которую объект атакует.
    public GameObj target;

    // Хранилище, в котором объект находится. Присваивается хранилищем при добавлении в него.
    public ObjectStorage parent;

    // Если true. то объект рисуется в gui_batch, т.е не скалируется с
    // остальными
    public boolean is_ui = false;

    // Созданные объектом ракеты. Или что либо еще.
    public Array<GameObj> createdObjects;

    // Сколько секунд живёт объект.
    public float lifetime;

    public float gun_cooldown = 0;

    // Nuff said
    public int hp = 20;

    // Nuff said
    public boolean isDead = false;

    public Vector2 clipping_point;

    public Variables() {
        pos = new Vector2();
        way = new Vector2();
        size = new Vector2();
        clipping_point = new Vector2();
        createdObjects = new Array<>();
    }

    public Rectangle getBounds() {
        Rectangle box = new Rectangle();

        box.setWidth(size.x);
        box.setHeight(size.y);
        box.setX(pos.x);
        box.setY(pos.y);

        return box;
    }

    /**
     * @return пол размера img
     */
    public Vector2 halfSize() {
        return size.cpy().div(2);
    }

    public Vector2 size() {
        return size.cpy();
    }

}
