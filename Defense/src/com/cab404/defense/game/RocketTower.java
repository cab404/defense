package com.cab404.defense.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.primitives.Particle;
import com.cab404.defense.primitives.Tower;
import com.cab404.defense.util.Def;
import com.cab404.defense.util.SpriteUtils;

/**
 * Ну вcё предельно ясно. Башня, стреляющая ракетами.
 *
 * @author cab404
 */
public class RocketTower extends Tower {

    public static class Rocket extends Particle {

        public static class TraceEmitter extends ParticleEmitter {

            public TraceEmitter(GameObj owner) {
                this.delay = 5 / owner.stats.speed;
                this.clipped_to = owner;
            }


            @Override
            public Particle getTemplate() {
                Particle prt = new Particle();
                prt.live = 0.5f;
                prt.stats.color = Color.GRAY.cpy();
                prt.var.way = clipped_to.var.way.cpy();
                prt.stats.speed = 0;
                return prt;
            }

        }

        public ParticleEmitter trace;


        public Rocket() {
            var = new Variables();
            stats = new Statistics();

            live = 5f;
            img = new Sprite(Def.sprites.get("main", "rocket"));
            var.way = new Vector2((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);
            SpriteUtils.sync(var, img);
        }


        @Override
        protected void move_iteration(float iteration) {
            trace.step(time / iteration, var.parent);
        }


        @Override
        public void update() {
            smoothRotation();
            move(true);
            if (var.lifetime > live)
                var.hp = 0;
        }


        @Override
        public void onDeath() {
            genExplosion(10);
        }

    }


    public RocketTower() {
        super();
        var.hp = 200;

        stats.firing_range = 500;
        stats.max_hp = 200;
        stats.speed = 5;
        stats.firing_speed = 3;
        stats.rotation_speed = 60 * 3;

        projectile_stats.speed = 300;
        projectile_stats.rotation_speed = 60 * 6;
        projectile_stats.color = Color.GREEN.cpy();
    }

    @Override
    public GameObj getProjectile() {
        Rocket rocket = new Rocket();
        centerObjectOnSelf(rocket);
        rocket.stats = projectile_stats;

        rocket.trace = new Rocket.TraceEmitter(rocket);

        return rocket;
    }


    @Override
    public void update() {
        var.target = findNearestIn(var.parent.getAllTeamsExcept(stats.team));
        sendAllRockets();
        retargetObjects();
        destroyFarRockets();
    }


}
