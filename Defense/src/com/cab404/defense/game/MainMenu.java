package com.cab404.defense.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.scene.Scene;
import com.cab404.defense.storage.SpriteStorage.TileStorage;
import com.cab404.defense.ui.Field;
import com.cab404.defense.ui.Menu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.ui.TextField;
import com.cab404.defense.util.Def;

/**
 * @author cab404
 */
public class MainMenu extends Scene {

    Vector2 mewnu_position = new Vector2();
    Menu mewnu;

    @Override
    public void create() {


        super.create();

        TileStorage main = new TileStorage("data/tiles.png");

        main.add(1, 24, 2, 2, "text bg");

        main.add(23, 0, 11, 13, "enemy");
        main.add(0, 0, 23, 23, "tower");

        main.add(0, 57, 5, 7, "rocket");
        main.add(5, 51, 5, 5, "laser");

        main.add(0, 52, 5, 5, "1 - particle");
        main.add(0, 47, 5, 5, "2 - particle");
        main.add(0, 42, 5, 5, "3 - particle");
        main.add(0, 37, 5, 5, "4 - particle");
        main.add(0, 32, 5, 5, "5 - particle");
        main.add(0, 27, 5, 5, "6 - particle");

        sprites.add("main", main);
        sprites.load();

        // Кидаем вручную, иначе ожидаются NullPointer в создании Field-ов (bg)
        // Вреда от этого никакого.
        Def.sprites = sprites;

        mewnu = new Menu();

        Field title = new TextField() {
            @Override
            public String getText() {
                return "Defense: 0.8a;";
            }

            @Override
            public boolean isDead() {
                return false;
            }

            @Override
            public void onMouseOver(Vector2 mouse_pos) {
            }
        };

        SelectableField new_game = new SelectableField() {

            @Override
            public String getText() {
                return " - New game;";
            }

            @Override
            public void onSelect(Vector2 mouse) {
                Def.scene.changeScene(new Game());
            }
        };

        mewnu.fields.add(new_game);
        mewnu.fields.add(title);
        mewnu.distance_between_fields = 5;

        objects.add(mewnu);
    }

    @Override
    public void update() {
        objects.update(1, Gdx.graphics.getDeltaTime());
        mewnu.var.pos.set(Scene.scr.cpy().div(2).sub(mewnu.var.halfSize()));
    }

    @Override
    public void resize(Vector2 size) {
        super.resize(size);
        mewnu.internalUpdate();
    }

    @Override
    public void render() {
        super.render();
        objects.render();
    }

}
