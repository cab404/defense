package com.cab404.defense.game;

import com.cab404.defense.primitives.GameObj;
import com.cab404.defense.primitives.Particle;
import com.cab404.defense.storage.ObjectStorage;

/**
 * Простейший класс, отвечающий за создание частиц через равные промежутки
 * времени
 *
 * @author cab404
 */
public abstract class ParticleEmitter {
    public float delay;
    protected float delayLeft;
    public GameObj clipped_to;


    public abstract Particle getTemplate();


    public void step(float time, ObjectStorage target) {
        if (clipped_to != null) {
            delayLeft += time;
            for (; delayLeft > delay; delayLeft -= delay) {
                Particle prt = getTemplate();
                clipped_to.centerObjectOnSelf(prt);
                target.add(prt);
            }
        }
    }
}
