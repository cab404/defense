package com.cab404.defense.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.primitives.Ship;
import com.cab404.defense.primitives.Tower;
import com.cab404.defense.scene.GameScene;
import com.cab404.defense.storage.SpriteStorage.TileStorage;
import com.cab404.defense.ui.Menu;
import com.cab404.defense.ui.SelectableField;
import com.cab404.defense.util.Def;

public class Game extends GameScene {

    @Override
    public void create() {
        super.create();

        // Загружаем тайлы

        TileStorage main = new TileStorage("data/tiles.png");

        main.add(1, 24, 2, 2, "text bg");

        main.add(23, 0, 11, 13, "enemy");
        main.add(0, 0, 23, 23, "tower");

        main.add(0, 57, 5, 7, "rocket");
        main.add(5, 51, 5, 5, "laser");

        main.add(0, 52, 5, 5, "1 - particle");
        main.add(0, 47, 5, 5, "2 - particle");
        main.add(0, 42, 5, 5, "3 - particle");
        main.add(0, 37, 5, 5, "4 - particle");
        main.add(0, 32, 5, 5, "5 - particle");
        main.add(0, 27, 5, 5, "6 - particle");

        sprites.add("main", main);
        sprites.load();

        Gdx.input.setInputProcessor(new GameSceneInputProcessor());

    }

    @Override
    public void update() {
        super.update();
        if (Gdx.input.isKeyPressed(Keys.NUM_1)) {
            time_zoom += 0.1f;
        }

        if (Gdx.input.isKeyPressed(Keys.NUM_2)) {
            time_zoom -= 0.1f;
            time_zoom = time_zoom < 0 ? 0 : time_zoom;
        }

        if (Gdx.input.isKeyPressed(Keys.NUM_3)) {
            time_zoom = 1;
        }

        if (Gdx.input.isKeyPressed(Keys.NUM_0)) {
            game.gfx.cam.direction.y += 0.001f;
            game.gfx.cam.direction.nor();
            game.gfx.cam.viewportHeight += 0.01f;
        }

        if (Gdx.input.isKeyPressed(Keys.UP)) {
            game.gfx.cam.translate(0, 5);
        }
        if (Gdx.input.isKeyPressed(Keys.DOWN)) {
            game.gfx.cam.translate(0, -5);
        }
        if (Gdx.input.isKeyPressed(Keys.RIGHT)) {
            game.gfx.cam.translate(5, 0);
        }
        if (Gdx.input.isKeyPressed(Keys.LEFT)) {
            game.gfx.cam.translate(-5, 0);
        }


        if (Gdx.input.justTouched()) {
            Vector2 mouse_pos = Def.mouse(0);
            final Vector2 spawn_pos = game.gfx.convertToGFX(mouse_pos);

            SelectableField laser_tower = new SelectableField() {

                @Override
                public void onSelect(Vector2 mouse) {
                    Tower twr = new LaserTower();

                    twr.var.pos = spawn_pos.cpy();
                    twr.stats.team = Color.BLUE;

                    Menu mn = new Menu();
                    mn.fields.add(new Tower.TowerInformationField(twr));
                    twr.internalUpdate();
                    mn.var.pos = twr.var.clipping_point;

                    game.add(twr);
                    objects.add(mn);
                }

                @Override
                public String getText() {
                    return "Laser tower";
                }

            };

            SelectableField ship = new SelectableField() {

                @Override
                public void onSelect(Vector2 mouse) {

                    Vector2 spawn_vector = new Vector2(1, 0);

                    Color team = new Color((float) Math.random(), 1, 1, 1);

                    for (int i = 0; i != 20; i++) {
                        Ship ship = new Ship();

                        ship.var.hp = 50;
                        ship.var.pos = spawn_pos.cpy().add(spawn_vector);
                        ship.stats.team = team;
                        ship.stats.color = team;

                        spawn_vector.rotate((float) Math.random() * 360);
                        spawn_vector.nor().mul((float) Math.random() * 100);

                        ship.stats.speed = 50;
                        ship.stats.firing_range = 200;
                        ship.stats.rotation_speed = 60 * 3;

                        ship.projectile_stats.speed = 100;
                        ship.projectile_stats.rotation_speed = 60 * 3;

                        game.add(ship);
                    }
                }

                @Override
                public String getText() {
                    return "Ship";
                }

            };

            SelectableField rocket_tower = new SelectableField() {

                @Override
                public void onSelect(Vector2 mouse) {
                    Tower twr = new RocketTower();
                    twr.var.pos = spawn_pos.cpy();

                    twr.stats.team = Color.BLUE;

                    Menu mn = new Menu();
                    mn.fields.add(new Tower.TowerInformationField(twr));
                    twr.internalUpdate();
                    mn.var.pos = twr.var.clipping_point;

                    game.add(twr);
                    objects.add(mn);
                }

                @Override
                public String getText() {
                    return "Rocket tower";
                }

            };

            rocket_tower.fast_selection = true;
            laser_tower.fast_selection = true;
            ship.fast_selection = true;

            Menu menu = new Menu();
            menu.var.pos = mouse_pos.cpy();

            menu.fields.add(rocket_tower);
            menu.fields.add(laser_tower);
            menu.fields.add(ship);

            objects.add(menu);

        }

    }

}
