package com.cab404.defense.game;

import com.badlogic.gdx.graphics.Color;

public class Statistics implements Cloneable {
// Нужно придумать замену для этого, и для Variables

    public float firing_speed = 1;
    public int damage = 1;
    public float firing_range = 700f;
    public Color color;
    public Color team;

    public boolean isStationary = false;
    public float speed = 60;
    public float rotation_speed = 60;

    public int max_hp = 20;
    public int hp_regen = 1;

    public Statistics() {
        color = Color.WHITE.cpy();
        team = Color.WHITE.cpy();
    }

}
