package com.cab404.defense.primitives;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.cab404.defense.util.Def;
import com.cab404.defense.util.SpriteUtils;

public class Particle extends GameObj {

    public float live = 1;
    public Sprite img;

    public Particle() {
        super();
        int rnd_num = (int) (Math.random() * 6) + 1;

        img = Def.sprites.get("main", rnd_num + " - particle");

        stats.speed = 60;
        SpriteUtils.sync(var, img);
    }


    @Override
    public void draw(SpriteBatch batch) {
        img.setRotation(var.way.angle() - 90);
        img.setPosition(var.pos.x, var.pos.y);
        img.setColor(stats.color);
        img.draw(batch);
    }


    @Override
    public void update() {

        if (live <= var.lifetime)
            var.hp = 0;
        else
            stats.color.a = (live - var.lifetime) / live;
        img.setColor(stats.color);
        move(false);

    }


    @Override
    public void onDeath() {

    }

}
