package com.cab404.defense.primitives;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.game.Statistics;
import com.cab404.defense.game.Variables;

import java.util.Iterator;

public abstract class GameObj {

    /**
     * Переменные элемента, часто меняются.
     */
    public Variables var;

    /**
     * Статы элемента. Меняются с левелапами и апгрейдами.
     */
    public Statistics stats;

    /**
     * Время с последнего кадра.
     */
    public static float time;


    /**
     * Прорисовка элемента.
     */
    public abstract void draw(SpriteBatch batch);


    /**
     * Обновление элемента.
     */
    public abstract void update();


    /**
     * Вызывается сразу после уничтожения объекта. isDead выставлять вручную не
     * нужно.
     */
    public void onDeath() {
    }


    public GameObj() {
        var = new Variables();
        stats = new Statistics();
    }


    public void genExplosion(int particles) {
        for (int i = 0; i != particles; i++) {
            Particle prt = new Particle();
            prt.stats.color = Math.random() > 0.5 ? Color.ORANGE.cpy() : Color.YELLOW.cpy();
            centerObjectOnSelf(prt);

            prt.var.way = new Vector2((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);

            prt.stats.speed = (float) (stats.speed / 3 * (Math.random() + 0.5f));
            prt.live = 0.5f;
            prt.var.way.nor().add(var.way.cpy().mul(2));

            var.parent.add(prt);
        }
    }


    public boolean overlaps(GameObj second_element) {
        Vector2 dot = var.pos.cpy().add(var.halfSize());

        boolean toReturn;
        float x = second_element.var.pos.x;
        float y = second_element.var.pos.y;
        float w = second_element.var.size.x;
        float h = second_element.var.size.y;

        toReturn = dot.x > x && dot.y > y && dot.x < x + w && dot.y < y + h;

        return toReturn;
    }


    public final void internalUpdate() {
        var.lifetime += time;

    }


    public void centerObjectOnSelf(GameObj obj) {
        obj.var.pos = var.pos.cpy();
        obj.var.pos.add(new Vector2(
                (var.size.x - obj.var.size.x) / 2,
                (var.size.y - obj.var.size.y) / 2));
    }


    /**
     * Плавно поворачивает объект в сторону цели. И это важно :)
     */
    public void smoothRotation() {


        if (var.target != null && !var.target.var.isDead) {

            float angleTT = var.target.var.pos.cpy() // Копируем позицию цели
                    .add(var.target.var.halfSize()) // Центруем позицию,
                            // добавляя пол размера цели
                    .sub(var.halfSize()) // ... и вычитая пол своего
                    .sub(var.pos).angle(); // Вычисляем угол

            float angle = var.way.angle();
            float delta = angle - angleTT;

            // Решение всяких переходов из -180 в +180 и т.д. Работает.

            if (delta > 180)
                delta -= 360;

            if (delta < -180)
                delta += 360;

            if (angle != angleTT) {

                if (Math.abs(delta) > stats.rotation_speed * time) {
                    if (delta < 0) {
                        var.way.rotate(stats.rotation_speed * time);
                    } else if (delta > 0) {
                        var.way.rotate(-stats.rotation_speed * time);
                    }

                } else {
                    var.way.rotate(-delta);
                }

            }

        }

    }


    /**
     * Двигает объект в направлении var.way со скоростью var.speed.
     *
     * @param kamikaze Если true - объект взорвётся при столкновении с var.target,
     *                 нанося stats.damage урона.
     */
    public void move(boolean kamikaze) {
        var.way = var.way.nor();

        if (kamikaze && var.target != null && !var.target.var.isDead) {
            float delta = time * stats.speed;
            float checkPrecision = 0.5f;
            float iterations = delta / checkPrecision;

            for (float i = 0; i < delta; i += checkPrecision) {
                move_iteration(iterations);
                var.pos.add(var.way.cpy().mul(checkPrecision));

                if (overlaps(var.target)) {
                    var.target.var.hp -= stats.damage;
                    var.hp = 0;
                    break;
                }

            }
        } else {
            float delta = time * stats.speed;
            float checkPrecision = 0.5f;
            float iterations = delta / checkPrecision;

            for (float i = 0; i < delta; i += checkPrecision) {
                move_iteration(iterations);
                var.pos.add(var.way.cpy().mul(checkPrecision));
            }
        }

    }


    /**
     * Выполняется в методе move каждую итерацию
     *
     * @param iterations Кол-во итераций за обновление
     */
    protected void move_iteration(float iterations) {
    }


    public boolean isInRange(GameObj obj, float range) {
        return obj.var.pos.dst2(var.pos) <= Math.pow(range, 2);
    }


    /**
     * Отправляет все снаряды, которые возможно, в var.target, при этом учитывая
     * stats.firing_speed
     */
    public void sendAllRockets() {


        if (var.target != null && !var.target.var.isDead &&
                isInRange(var.target, stats.firing_range)) {
            // Если кто-то в радиусе экстерминации - экстерминатус!

            var.gun_cooldown += time;

            float interval = 1f / stats.firing_speed;

            for (; var.gun_cooldown >= interval; var.gun_cooldown -= interval) {
                GameObj proj = getProjectile();
                var.createdObjects.add(proj);
                var.parent.add(proj);
            }
        } else {
            // А иначе - точим ножи на потенциальную жертву :D
            var.gun_cooldown += time;
            var.gun_cooldown =
                    var.gun_cooldown > 1 / stats.firing_speed
                            ? 1 / stats.firing_speed
                            : var.gun_cooldown;
        }
    }


    /**
     * В заданном массиве объектов ищет ближайший.
     *
     * @param array Массив для поиска
     * @return Ближайший объект к данному
     */

    public GameObj findNearestIn(Array<GameObj> array) {


        Iterator<GameObj> objects = array.iterator();
        GameObj nearest = null;
        float dist_to_nearest = 0;

        while (objects.hasNext()) {
            GameObj to_check = objects.next();

            if (nearest == null || dist_to_nearest > to_check.var.pos.dst2(var.pos)) {
                nearest = to_check;
                dist_to_nearest = to_check.var.pos.dst2(var.pos);
            }
        }


        if (nearest != null &&
                isInRange(nearest, stats.firing_range)) {
            var.target = nearest;
        }


        return nearest;

    }


    /**
     * Перенаправляет снаряды на новую цель.
     */
    public void retargetObjects() {

        for (GameObj createdObject : var.createdObjects) {
            createdObject.var.target = var.target;
        }

    }


    public GameObj getProjectile() {
        return null;
    }


    /**
     * Вызывается в update(), когда курсор находится в пределах var.img
     */
    public void onMouseOver(Vector2 mousePos) {
    }


    /**
     * Уничтожает ракету, если улетела черезчер далеко.
     */
    public void destroyFarRockets() {

        for (GameObj tmp : var.createdObjects) {
            float range = stats.firing_range * stats.firing_range * 2;

            if (tmp.var.pos.dst2(var.pos) > range) {
                tmp.var.hp = 0;
            }

        }

    }


    public String toString() {
        return this.getClass().toString() + ":" + this.var.pos.x + ":" + var.pos.y;
    }
}
