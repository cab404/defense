package com.cab404.defense.primitives;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.cab404.defense.game.Statistics;
import com.cab404.defense.ui.TextField;
import com.cab404.defense.util.Def;
import com.cab404.defense.util.SpriteUtils;

public abstract class Tower extends GameObj {
    protected Array<GameObj> created_objects;
    public Statistics projectile_stats;
    public Sprite img;

    public static class TowerInformationField extends TextField {
        Tower tower;


        public TowerInformationField(Tower source) {
            super();
            tower = source;
        }


        @Override
        public void onMouseOver(Vector2 mouse_pos) {
        }


        @Override
        public String getText() {
            return "HP: " + tower.var.hp + "/" + tower.stats.max_hp + "\n";
        }


        @Override
        public boolean isDead() {
            return tower.var.isDead;
        }

    }


    public Tower() {
        super();
        projectile_stats = new Statistics();
        created_objects = new Array<>();
        img = Def.sprites.get("main", "tower");
        SpriteUtils.sync(var, img);
    }


    @Override
    public void draw(SpriteBatch batch) {
        img.setRotation(var.way.angle() - 90);
        img.setColor(stats.color);
        img.setPosition(var.pos.x, var.pos.y);
        img.draw(batch);
    }


    // Всякие переменные для метода ниже
    private float curAngle = 0;


    /**
     * Скажу прямо - классный круг из частиц, обозначающий дальность башни.
     */
    public void drawRangeCircle() {

        Vector2 range = new Vector2(0, stats.firing_range);
        range.rotate(curAngle);

        float p = stats.firing_range * (float) Math.PI;
        float particle_lifetime = 2f * 60 * time;

        // Количество орбов (движущихся точек, на которых создаются частицы)
        int orbs = (int) (p / 500);

        for (int i = 0; i != orbs; i++) {
            Particle prt = new Particle();
            prt.live = particle_lifetime;
            prt.var.pos = range.cpy().add(var.pos);
            prt.stats.color = projectile_stats.color.cpy();
            prt.img.setRotation(range.angle());

            var.parent.add(prt);

            range.rotate(360 / orbs);
        }

        curAngle += 360 / (p / 5);
    }


    @Override
    public void onMouseOver(Vector2 mouse) {
        drawRangeCircle();
    }


    @Override
    public void onDeath() {
        for (int i = 1; i != 100; i++) {
            Particle prt = new Particle();

            prt.live = (float) (4 + (Math.random() - 0.5f) * 1f);
            prt.var.pos = var.pos.cpy().add(var.halfSize());
            prt.stats.speed = 500 * (float) Math.random();

            prt.var.way = new Vector2
                    ((float) Math.random() - 0.5f, (float) Math.random() - 0.5f);


            var.parent.add(prt);
        }
    }

}
