package com.cab404.defense.primitives;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.cab404.defense.game.RocketTower;
import com.cab404.defense.game.RocketTower.Rocket;
import com.cab404.defense.game.Statistics;
import com.cab404.defense.util.Def;
import com.cab404.defense.util.SpriteUtils;


public class Ship extends GameObj {

    public Statistics projectile_stats;
    public Sprite img;

    public Ship() {
        super();
        projectile_stats = new Statistics();
        projectile_stats.color = Color.RED.cpy();
        img = Def.sprites.get("main", "enemy");
        var.way = new Vector2(1, 1);
    }


    @Override
    public void draw(SpriteBatch batch) {
        img.setRotation(var.way.angle() - 90);
        img.setPosition(var.pos.x, var.pos.y);
        img.setColor(stats.color);
        img.draw(batch);
        SpriteUtils.sync(var, img);
    }


    @Override
    public void update() {
        var.target = findNearestIn(var.parent.getAllTeamsExcept(stats.team));
        smoothRotation();
        move(false);
        sendAllRockets();
        destroyFarRockets();
    }


    @Override
    public GameObj getProjectile() {
        Rocket proj = new RocketTower.Rocket();
        centerObjectOnSelf(proj);

        proj.var.target = var.target;
        proj.stats = projectile_stats;

        proj.trace = new Rocket.TraceEmitter(proj);

        return proj;
    }

    @Override
    public void onDeath() {
        genExplosion(10);
    }

}
