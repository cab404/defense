package com.cab404.defense;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.cab404.defense.game.MainMenu;
import com.cab404.defense.scene.SceneManager;
import com.cab404.defense.util.Def;

public class DefenseAndroid extends AndroidApplication {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;

        Def.scene = new SceneManager(new MainMenu());
        initialize(Def.scene, cfg);
    }
}