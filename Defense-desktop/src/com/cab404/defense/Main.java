package com.cab404.defense;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.cab404.defense.game.MainMenu;
import com.cab404.defense.scene.SceneManager;
import com.cab404.defense.util.Def;

public class Main {
    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "";
        cfg.useGL20 = false;
        cfg.width = 800;
        cfg.height = 600;

        Def.scene = new SceneManager(new MainMenu());
        new LwjglApplication(Def.scene, cfg);
    }
}
